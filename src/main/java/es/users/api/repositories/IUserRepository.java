package es.users.api.repositories;

import es.users.api.entities.TmUser;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Frank
 */
public interface IUserRepository extends CrudRepository<TmUser, Long> {

}
