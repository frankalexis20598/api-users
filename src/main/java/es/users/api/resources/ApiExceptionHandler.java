package es.users.api.resources;

import es.users.api.exceptions.ResourceNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 *
 * @author Frank
 */
@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler({MethodArgumentTypeMismatchException.class, HttpMessageNotReadableException.class})
    public ResponseEntity<ErrorMessage> handleBadRequestException(HttpServletRequest request, Exception e) {
        ErrorMessage errorMessage = new ErrorMessage("Bad Request", HttpStatus.BAD_REQUEST.value(), request.getRequestURI());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleApiRequestException(HttpServletRequest request, ResourceNotFoundException e) {
        ErrorMessage errorMessage = new ErrorMessage(e.getMessage(), HttpStatus.NOT_FOUND.value(), request.getRequestURI());
        return new ResponseEntity<>(errorMessage, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorMessage> handleInvalidInputException(HttpServletRequest request, MethodArgumentNotValidException e) {
        List<InputErrors> inputErrors = e.getBindingResult().getFieldErrors()
            .stream()
            .map(x -> {
                String message = "El campo " + x.getField() + " " + x.getDefaultMessage();
                return new InputErrors(x.getField(), message);
            })
            .collect(Collectors.toList());

        ValidationErrorMessage validationErrorMessage = new ValidationErrorMessage(
            "Invalid input",
            HttpStatus.METHOD_NOT_ALLOWED.value(),
            request.getRequestURI(),
            inputErrors);

        return new ResponseEntity<>(validationErrorMessage, HttpStatus.METHOD_NOT_ALLOWED);
    }

}
