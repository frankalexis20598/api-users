package es.users.api.resources;

import java.util.List;

/**
 *
 * @author Frank
 */
public class ValidationErrorMessage extends ErrorMessage {
    
    private List<InputErrors> inputErrors;
    
    public ValidationErrorMessage(String message, int status, String uriRequested, List<InputErrors> inputErrors) {
        super(message, status, uriRequested);
        this.inputErrors = inputErrors;
    }

    public List<InputErrors> getInputErrors() {
        return inputErrors;
    }
    
}
