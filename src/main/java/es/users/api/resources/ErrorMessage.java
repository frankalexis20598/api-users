package es.users.api.resources;

import java.io.Serializable;

/**
 *
 * @author Frank
 */
public class ErrorMessage implements Serializable {
    
    private String message;
    private int status;
    private String path;
    
    public ErrorMessage(String message, int status, String uriRequested) {
        this.message = message;
        this.status = status;
        this.path = uriRequested;
    }
    
    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }

    public String getPath() {
        return path;
    }
    
}
