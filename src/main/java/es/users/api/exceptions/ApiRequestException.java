package es.users.api.exceptions;

import org.springframework.http.HttpStatus;

/**
 *
 * @author Frank
 */
public class ApiRequestException extends RuntimeException {
    
    private HttpStatus statusCode;
    
    public ApiRequestException(String message, HttpStatus statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    public ApiRequestException(String message, Throwable throwable, HttpStatus statusCode) {
        super(message, throwable);
        this.statusCode = statusCode;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }
    
}
