package es.users.api.exceptions;

/**
 *
 * @author Frank
 */
public class ResourceNotFoundException extends Exception {
    
    private static final long serialVersionUID = 1L;
    
    public ResourceNotFoundException(String message) {
        super(message);
    }
    
}
