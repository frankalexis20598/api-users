package es.users.api.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Frank
 */
@Entity
@Table(name = "tm_address")
public class TmAddress implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = true)
    @NotNull
    @Size(max = 255)
    private String street;

    @Column(name = "txt_state", nullable = true)
    @NotNull
    @Size(max = 255)
    private String state;

    @Column(nullable = true)
    @NotNull
    @Size(max = 255)
    private String city;

    @Column(nullable = true)
    @NotNull
    @Size(max = 255)
    private String country;

    @Column(nullable = true)
    @NotNull
    @Size(max = 255)
    private String zip;

    public TmAddress() {
    }

    public TmAddress(Long id, String street, String state, String city, String country, String zip) {
        this.id = id;
        this.street = street;
        this.state = state;
        this.city = city;
        this.country = country;
        this.zip = zip;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TmAddress other = (TmAddress) obj;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return "AddressEntity{" + "id=" + id + ", street=" + street + ", state=" + state + ", city=" + city + ", country=" + country + ", zip=" + zip + '}';
    }

}
