package es.users.api.services;

import es.users.api.entities.TmUser;
import es.users.api.repositories.IUserRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Frank
 */
@Service
public class UserServiceImpl implements IUserService {
    
    @Autowired
    private IUserRepository userRepository;
    
    @Override
    public List<TmUser> findAll() {
        List<TmUser> listUsers = (List<TmUser>) userRepository.findAll();
        return listUsers
            .stream()
            .sorted((o1, o2) -> o1.getId().compareTo(o2.getId()))
            .collect(Collectors.toList());
    }

    @Override
    public TmUser save(TmUser user) {
        return userRepository.save(user);
    }
    
    @Override
    public Optional<TmUser> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public void delete(TmUser user) {
        userRepository.delete(user);
    }

}
