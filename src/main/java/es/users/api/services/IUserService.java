/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.users.api.services;

import es.users.api.entities.TmUser;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Frank
 */
public interface IUserService {
    
    public List<TmUser> findAll();
    
    public TmUser save(TmUser user);
    
    public Optional<TmUser> findById(Long id);
    
    public void delete(TmUser user);
    
}
