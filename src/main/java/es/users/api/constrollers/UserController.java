package es.users.api.constrollers;

import es.users.api.entities.TmUser;
import es.users.api.exceptions.ResourceNotFoundException;
import es.users.api.services.IUserService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Frank
 */
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private IUserService userService;

    /**
     * Obtiene todos los usuarios.
     *
     * @return lista de usuarios.
     */
    @GetMapping(
        value = "/getusers",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TmUser> getUsers() {
        return userService.findAll();
    }

    /**
     * Crea un nuevo usuario.
     *
     * @param user datos del usuario.
     * @return usuario creado.
     */
    @PostMapping(
        value = "/createUsers",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TmUser> createUsers(@Valid @RequestBody TmUser user) {
        TmUser response = userService.save(user);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    /**
     * Obtiene un usuario por el número de Id.
     *
     * @param userId id del usuario.
     * @return usuario encontrado.
     * @throws ResourceNotFoundException
     */
    @GetMapping(
        value = "/getusersById/{userId}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TmUser> getUserById(@PathVariable("userId") Integer userId) throws ResourceNotFoundException {
        TmUser user = userService.findById(Long.valueOf(userId))
            .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        return ResponseEntity.ok(user);
    }

    /**
     * Actualiza un usuario por el número de Id.
     *
     * @param userId id del usuario.
     * @param user datos del usuario.
     * @return usuario actualizado.
     * @throws ResourceNotFoundException
     */
    @PutMapping(
        value = "/updateUsersById/{userId}",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TmUser> updateUsersById(@PathVariable("userId") Integer userId, @Valid @RequestBody TmUser user) throws ResourceNotFoundException {
        TmUser tmUser = userService.findById(Long.valueOf(userId))
            .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        tmUser.setName(user.getName());
        tmUser.setEmail(user.getEmail());
        tmUser.setBirthDate(user.getBirthDate());
        tmUser.setAddress(user.getAddress());
        TmUser response = userService.save(tmUser);
        return ResponseEntity.ok(response);
    }

    /**
     * Elimina un usuario por el número de Id.
     *
     * @param userId id del usuario.
     * @return httpstatus.
     * @throws ResourceNotFoundException
     */
    @DeleteMapping(
        value = "/deleteUsersById/{userId}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteUsersById(@PathVariable("userId") Integer userId) throws ResourceNotFoundException {
        TmUser user = userService.findById(Long.valueOf(userId))
            .orElseThrow(() -> new ResourceNotFoundException("User with id " + userId + " not found"));
        userService.delete(user);
        return new ResponseEntity(HttpStatus.OK);
    }
}
